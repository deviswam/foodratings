//
//  EstablishmentTests.swift
//  FoodRatings
//
//  Created by Waheed Malik on 20/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FoodRatings

class EstablishmentTests: XCTestCase {
    func testConformanceToEstablishmentProtocol() {
        // Arrange
        let sut = FREstablishment(id: 0)
        // Assert
        XCTAssert((sut as Any) is Establishment)
    }
    
    func testInit_establishmentMustBeCreatedWithAnId() {
        // Arrange
        let establishmentId = 123
        let sut = FREstablishment(id: establishmentId)
        // Assert
        XCTAssertEqual(sut.id, establishmentId)
    }
    
    func testEstablishmentShouldHaveARatingValue() {
        // Arrange
        let establishmentId = 123
        let ratingValue = "Pass"
        let sut = FREstablishment(id: establishmentId, ratingValue: ratingValue)
        // Assert
        XCTAssertEqual(sut.ratingValue, ratingValue)
    }
    
    func testEstablishmentsForEquality_withSameId_areSameEstablishments() {
        //Arrange
        let est1 = FREstablishment(id: 123, ratingValue: "5")
        let est2 = FREstablishment(id: 123)
        
        //Assert
        XCTAssert(est1 == est2, "Both are same establishment")
    }
    
    func testEstablishmentConformsToObjectMapperProtocol() {
        //Arrange
        let sut = FREstablishment(id: 123)
        
        //Assert
        XCTAssertTrue((sut as Any) is ObjectMapper, "Establishment should conforms to ObjectMapper protocol")
    }
    
    func testInit_withNilDictionary_shouldFailInitialization() {
        //Arrange
        let sut = FREstablishment(dictionary: nil)
        
        //Assert
        XCTAssertNil(sut, "Establishment should be created only with valid json dictionary")
    }
    
    func testInit_withNoFHRSIDInDictionary_shouldFailInitialization() {
        //Arrange
        let jsonEstablishmentDictionary = ["RatingValue":"1"]
        let sut = FREstablishment(dictionary: jsonEstablishmentDictionary as Dictionary<String, AnyObject>?)
        //Assert
        XCTAssertNil(sut, "Establishment should only be created if dictionary has FHRSID")
    }
    
    func testInit_withNoRatingValueInDictionary_shouldNotFailInitialization() {
        //Arrange
        let jsonEstablishmentDictionary = ["FHRSID":123]
        let sut = FREstablishment(dictionary: jsonEstablishmentDictionary as Dictionary<String, AnyObject>?)
        
        //Assert
        XCTAssertNotNil(sut, "Only FHRSID is mandatory to create Establishment object")
    }
    
    func testInit_withFHRSIDAndRatingValueInDictionary_shouldSetEstablishmentIdAndRatingValue() {
        //Arrange
        let jsonEstablishmentDictionary = ["FHRSID":123, "RatingValue":"1"] as [String : Any]
        let sut = FREstablishment(dictionary: jsonEstablishmentDictionary as Dictionary<String, AnyObject>?)
        
        //Assert
        XCTAssertEqual(sut?.id, 123)
        XCTAssertEqual(sut?.ratingValue, "1")
    }
}
