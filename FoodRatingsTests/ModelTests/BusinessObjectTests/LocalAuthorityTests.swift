//
//  LocalAuthorityTests.swift
//  FoodRatings
//
//  Created by Waheed Malik on 20/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FoodRatings

class LocalAuthorityTests: XCTestCase {
    func testConformanceToLocalAuthorityProtocol() {
        // Arrange
        let sut = FRLocalAuthority(id: 123)
        // Assert
        XCTAssert((sut as Any) is LocalAuthority)
    }
    
    func testInit_localAuthorityMustBeCreatedWithAnId() {
        // Arrange
        let localAuthorityId = 123
        let sut = FRLocalAuthority(id: localAuthorityId)
        // Assert
        XCTAssertEqual(sut.id, localAuthorityId)
    }
    
    func testInit_localAuthorityShouldHaveAName() {
        // Arrange
        let localAuthorityName = "Aberdeen City"
        let sut = FRLocalAuthority(id: 123, name: localAuthorityName)
        // Assert
        XCTAssertEqual(sut.name, localAuthorityName)
    }
    
    func testInit_localAuthorityShouldHaveARatingsCollection() {
        // Arrange
        let ratings = [FRLocalAuthorityRating(ratingValue: "Pass", percentageOfEstablishmentsInvolved: 25)]
        var sut = FRLocalAuthority(id: 456)
        sut.ratings = ratings
        // Assert
        XCTAssertNotNil(sut.ratings)
        XCTAssertEqual(sut.ratings?.first?.ratingValue, "Pass")
        XCTAssertEqual(sut.ratings?.first?.percentageOfEstablishmentsInvolved, 25)
    }
    
    func testLocalAuthorityConformsToObjectMapperProtocol() {
        //Arrange
        let sut = FRLocalAuthority(id: 123)
        
        //Assert
        XCTAssertTrue((sut as Any) is ObjectMapper, "LocalAuthority should conforms to ObjectMapper protocol")
    }
    
    func testInit_withNilDictionary_shouldFailInitialization() {
        //Arrange
        let sut = FRLocalAuthority(dictionary: nil)
        
        //Assert
        XCTAssertNil(sut, "LocalAuthority should be created only with valid json dictionary")
    }
    
    func testInit_withNoLocalAuthorityIdInDictionary_shouldFailInitialization() {
        //Arrange
        let jsonLocalAuthorityDictionary = ["Name":"Aberdeen City"]
        let sut = FRLocalAuthority(dictionary: jsonLocalAuthorityDictionary as Dictionary<String, AnyObject>?)
        //Assert
        XCTAssertNil(sut, "LocalAuthority should only be created if dictionary has LocalAuthorityId")
    }
    
    func testInit_withNoNameInDictionary_shouldNotFailInitialization() {
        //Arrange
        let jsonLocalAuthorityDictionary = ["LocalAuthorityId":123]
        let sut = FRLocalAuthority(dictionary: jsonLocalAuthorityDictionary as Dictionary<String, AnyObject>?)
        
        //Assert
        XCTAssertNotNil(sut, "Only LocalAuthorityId is mandatory to create LocalAuthority object")
    }

    func testInit_withLocalAuthorityIdAndNameInDictionary_shouldSetAuthoryIdAndName() {
        //Arrange
        let jsonLocalAuthorityDictionary = ["LocalAuthorityId":123, "Name":"Aberdeen City"] as [String: Any]
        let sut = FRLocalAuthority(dictionary: jsonLocalAuthorityDictionary as Dictionary<String, AnyObject>?)
        
        //Assert
        XCTAssertEqual(sut?.id, 123)
        XCTAssertEqual(sut?.name, "Aberdeen City")
    }
}
