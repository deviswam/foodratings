//
//  FoodRatingsManagerTests.swift
//  FoodRatings
//
//  Created by Waheed Malik on 21/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FoodRatings

extension FoodRatingsManagerTests {
    class MockFoodRatingsAPI: FoodRatingsAPI {
        var fetchLocalAuthoritiesCalled: Bool = false
        var completionHandler: (([LocalAuthority]?, FoodRatingsAPIError?) -> Void)?
        func fetchLocalAuthorities(completionHandler: @escaping (_ localAuthorities: [LocalAuthority]?, _ error: FoodRatingsAPIError?) -> Void) {
            self.fetchLocalAuthoritiesCalled = true
            self.completionHandler = completionHandler
        }
        
        func fetchEstablishments(of localAuthorityId: Int, completionHandler: @escaping ([Establishment]?, FoodRatingsAPIError?) -> Void) {

        }
    }
}

class FoodRatingsManagerTests: XCTestCase {
    var mockfoodRatingsAPI: MockFoodRatingsAPI!
    var sut: FRFoodRatingsManager!
    
    override func setUp() {
        super.setUp()
        mockfoodRatingsAPI = MockFoodRatingsAPI()
        sut = FRFoodRatingsManager(foodRatingsAPI: mockfoodRatingsAPI)
    }
    
    func testConformanceToFoodRatingsManagerProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is FoodRatingsManager, "FRFoodRatingsManager should conforms to FoodRatingsManager protocol")
    }
    
    func testLoadLocalAuthorities_asksFoodRatingsAPIToFetchLocalAuthorities() {
        // Act
        sut.loadLocalAuthorities { (localAuthorities, error) in
            
        }
        
        // Assert
        XCTAssert(mockfoodRatingsAPI.fetchLocalAuthoritiesCalled)
    }
    
    func testLoadLocalAuthorities_whenLocalAuthoritiesFound_shouldReceiveLocalAuthoritiesCollectionWithNoError() {
        // Arrange
        var receivedLocalAuthorities : [LocalAuthority]?
        var receivedError : FoodRatingsManagerError?
        let expectedLocalAuthorities = [FRLocalAuthority(id: 123)]
        
        // Act
        sut.loadLocalAuthorities { (localAuthorities, error) in
            receivedLocalAuthorities = localAuthorities
            receivedError = error
        }
        mockfoodRatingsAPI.completionHandler?(expectedLocalAuthorities, nil)
        
        // Assert
        XCTAssertNil(receivedError)
        XCTAssertNotNil(receivedLocalAuthorities)
        XCTAssert(expectedLocalAuthorities.first! == receivedLocalAuthorities!.first!)
    }
}
