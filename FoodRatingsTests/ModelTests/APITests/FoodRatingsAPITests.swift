//
//  FoodRatingsAPITests.swift
//  FoodRatings
//
//  Created by Waheed Malik on 21/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FoodRatings

extension FoodRatingsAPITests {
    class MockURLSession: URLSession {
        typealias Handler = (Data?, URLResponse?, Error?) -> Void
        
        var dataTaskRequestMethodCalled = false
        var urlRequest : URLRequest?
        var completionHandler : Handler?
        var dataTask = MockURLSessionDataTask()
        
        override func dataTask(with urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            dataTaskRequestMethodCalled = true
            self.urlRequest = urlRequest
            self.completionHandler = completionHandler
            return dataTask
        }
    }
    
    class MockURLSessionDataTask : URLSessionDataTask {
        var resumeGotCalled = false
        override func resume() {
            resumeGotCalled = true
        }
    }
}

class FoodRatingsAPITests: XCTestCase {
    var mockURLSession: MockURLSession!
    var sut: FRFoodRatingsAPI!
    
    override func setUp() {
        super.setUp()
        mockURLSession = MockURLSession()
        sut = FRFoodRatingsAPI(urlSession: mockURLSession)
    }
    
    func testConformanceToFoodRatingsAPIProtocol() {
        XCTAssertTrue((sut as AnyObject) is FoodRatingsAPI)
    }
    
    func testFetchLocalAuthorities_ShouldAskURLSessionForFetchingLocalAuthoritiesData() {
        // Act
        sut.fetchLocalAuthorities { (localAuthorities, error) in
            
        }
        
        // Assert
        XCTAssert(mockURLSession.dataTaskRequestMethodCalled)
    }
    
    func testFetchLocalAuthorities_WithValidJSONOfALocalAuthority_ShouldReturnALocalAuthorityObject() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let localAuthoritiesAPIResponseInString = "{\"authorities\":[{" +
            "\"LocalAuthorityId\":197," +
            "\"Name\":\"Aberdeen City\"" +
        "}]}"
        
        let responseData = localAuthoritiesAPIResponseInString.data(using: .utf8)
        let expectedLocalAuthorityId = 197
        var receivedLocalAuthority : LocalAuthority?
        var receivedError : FoodRatingsAPIError?
        
        //Act
        sut.fetchLocalAuthorities { (localAuthorities: [LocalAuthority]?, error: FoodRatingsAPIError?) in
            receivedLocalAuthority = localAuthorities?.first
            receivedError = error
            asynExpectation.fulfill()
        }
        
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedLocalAuthority?.id == expectedLocalAuthorityId, "Fetched and expected authority should have same id")
            XCTAssertTrue(receivedLocalAuthority?.name == "Aberdeen City", "Fetched and expected authority should have same name")
            XCTAssertNil(receivedError, "Nil should be returned as an Error along with authority object")
        }
    }
    
    func testFetchLocalAuthorities_WithValidJSONOfTwoLocalAuthorities_ShouldReturnTwoLocalAuthorityObjects() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let localAuthoritiesAPIResponseInString = "{\"authorities\":[{" +
            "\"LocalAuthorityId\":197," +
            "\"Name\":\"Aberdeen City\"" +
            "},{" +
            "\"LocalAuthorityId\":198," +
            "\"Name\":\"Aberdeenshire\"" +
        "}]}"
        
        let responseData = localAuthoritiesAPIResponseInString.data(using: .utf8)
        var receivedLocalAuthorities : [LocalAuthority]?
        var receivedError : FoodRatingsAPIError?
        
        //Act
        sut.fetchLocalAuthorities { (localAuthorities: [LocalAuthority]?, error: FoodRatingsAPIError?) in
            receivedLocalAuthorities = localAuthorities
            receivedError = error
            asynExpectation.fulfill()
        }
        
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertNil(receivedError, "Nil should be returned as an Error along with authority object")
            XCTAssertTrue(receivedLocalAuthorities?[0].id == 197, "Fetched and expected authority should have same id")
            XCTAssertTrue(receivedLocalAuthorities?[0].name == "Aberdeen City", "Fetched and expected authority should have same name")
            XCTAssertTrue(receivedLocalAuthorities?[1].id == 198, "Fetched and expected authority should have same id")
            XCTAssertTrue(receivedLocalAuthorities?[1].name == "Aberdeenshire", "Fetched and expected authority should have same name")
        }
    }
}
