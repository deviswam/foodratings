//
//  AuthorityTableViewCellTests.swift
//  FoodRatings
//
//  Created by Waheed Malik on 21/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FoodRatings

extension AuthorityTableViewCellTests {
    class FakeDataSource: NSObject, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return FRAuthorityTableViewCell()
        }
    }
}

class AuthorityTableViewCellTests: XCTestCase {
    let fakeDataSource = FakeDataSource()
    var tableView : UITableView!
    var cell : FRAuthorityTableViewCell!
    
    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let listAuthoritiesVC = storyBoard.instantiateViewController(withIdentifier: "ListAuthoritiesVC") as! ListAuthoritiesVC
        listAuthoritiesVC.authoritiesTableViewDataSource = fakeDataSource
        UIApplication.shared.keyWindow?.rootViewController = listAuthoritiesVC
        _ = listAuthoritiesVC.view
        tableView = listAuthoritiesVC.authoritiesTableView
        cell = tableView.dequeueReusableCell(withIdentifier: "AuthorityTableViewCell", for: IndexPath(row: 0, section: 0)) as! FRAuthorityTableViewCell
    }
    
    func testAuthorityTableViewCell_HasNameLabel() {
        // Assert
        XCTAssertNotNil(cell.authorityNameLabel)
    }
    
    func testConfigCell_setAuthorityNameOnLabel() {
        //Act
        cell.configCell(with: FRLocalAuthorityViewModel(localAuthority: FRLocalAuthority(id: 123, name: "Aberdeen City")))
        
        //Assert
        XCTAssertEqual(cell.authorityNameLabel.text!, "Aberdeen City")
    }
}
