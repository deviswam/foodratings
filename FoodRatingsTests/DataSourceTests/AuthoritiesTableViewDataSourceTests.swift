//
//  AuthoritiesTableViewDataSourceTests.swift
//  FoodRatings
//
//  Created by Waheed Malik on 21/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FoodRatings

extension AuthoritiesTableViewDataSourceTests {
    class MockTableView: UITableView {
        var cellGotDequeued = false
        override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        }
    }
    
    class MockAuthorityTableViewCell: UITableViewCell, AuthorityTableViewCell {
        var configCellGotCalled = false
        var localAuthority : LocalAuthorityViewModel?
        func configCell(with authorityViewModel: LocalAuthorityViewModel) {
            configCellGotCalled = true
            self.localAuthority = authorityViewModel
        }
    }
    
    class MockListAuthoritiesViewModel: ListAuthoritiesViewModel {
        var localAuthorities: [LocalAuthority]?
        func numberOfLocalAuthorities() -> Int {
            return localAuthorities!.count
        }
        
        func localAuthority(at index: Int) -> LocalAuthorityViewModel? {
            return FRLocalAuthorityViewModel(localAuthority: localAuthorities![index])
        }
        
        func getLocalAuthorities() {
        }
    
        func selectedLocalAuthority(at index: Int) {
        }
    }
}

class AuthoritiesTableViewDataSourceTests: XCTestCase {
    func testDataSourceHasListAuthoritiesViewModel() {
        // Arrange
        let sut = AuthoritiesTableViewDataSource()
        sut.viewModel = MockListAuthoritiesViewModel()
        // Assert
        XCTAssertNotNil(sut.viewModel)
        XCTAssertNotNil((sut.viewModel! as Any) is ListAuthoritiesViewModel)
    }
    
    func testNumberOfRowsInTheSection_WithTwoAuthorities_ShouldReturnTwo() {
        //Arrange
        let sut = AuthoritiesTableViewDataSource()
        let authorities = [FRLocalAuthority(id: 123), FRLocalAuthority(id: 456)]
        let mockListAuthoritiesViewModel = MockListAuthoritiesViewModel()
        mockListAuthoritiesViewModel.localAuthorities = authorities
        
        sut.viewModel = mockListAuthoritiesViewModel
        let mockTableView = MockTableView()
        
        //Act
        let noOfItemsInSectionZero = sut.tableView(mockTableView, numberOfRowsInSection: 0)
        
        //Assert
        XCTAssertEqual(noOfItemsInSectionZero, 2, "Number of items in section 0 are 2")
    }
    
    func testCellForRow_ReturnsAuthorityTableViewCell() {
        //Arrange
        let sut = AuthoritiesTableViewDataSource()
        let authorities = [FRLocalAuthority(id: 123), FRLocalAuthority(id: 456)]
        let mockListAuthoritiesViewModel = MockListAuthoritiesViewModel()
        mockListAuthoritiesViewModel.localAuthorities = authorities
        
        sut.viewModel = mockListAuthoritiesViewModel
        let mockTableView = MockTableView(frame: CGRect.zero)
        
        mockTableView.dataSource = sut
        mockTableView.register(MockAuthorityTableViewCell.self, forCellReuseIdentifier: "AuthorityTableViewCell")
        
        //Act
        let cell = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(cell is AuthorityTableViewCell,"Returned cell is of AuthorityTableViewCell type")
    }
    
    func testCellForRow_DequeuesCell() {
        //Arrange
        let sut = AuthoritiesTableViewDataSource()
        let authorities = [FRLocalAuthority(id: 123), FRLocalAuthority(id: 456)]
        let mockListAuthoritiesViewModel = MockListAuthoritiesViewModel()
        mockListAuthoritiesViewModel.localAuthorities = authorities
        
        sut.viewModel = mockListAuthoritiesViewModel
        let mockTableView = MockTableView(frame: CGRect.zero)
        
        mockTableView.dataSource = sut
        mockTableView.register(MockAuthorityTableViewCell.self, forCellReuseIdentifier: "AuthorityTableViewCell")
        
        //Act
        _ = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(mockTableView.cellGotDequeued,"CellForRow should be calling DequeueCell method")
    }
    
    func testConfigCell_GetsCalledFromCellForRow() {
        //Arrange
        let sut = AuthoritiesTableViewDataSource()
        let expectedAuthority = FRLocalAuthority(id: 123, name: "Coventry")
        let authorities = [expectedAuthority]
        let mockListAuthoritiesViewModel = MockListAuthoritiesViewModel()
        mockListAuthoritiesViewModel.localAuthorities = authorities
        
        sut.viewModel = mockListAuthoritiesViewModel
        let mockTableView = MockTableView(frame: CGRect.zero)
        
        mockTableView.dataSource = sut
        mockTableView.register(MockAuthorityTableViewCell.self, forCellReuseIdentifier: "AuthorityTableViewCell")
        
        //Act
        let cell = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0)) as! MockAuthorityTableViewCell
        
        //Assert
        XCTAssertTrue(cell.configCellGotCalled,"CellForRow should be calling ConfigCell method")
        XCTAssertTrue(cell.localAuthority?.name == expectedAuthority.name, "Authority name should be same")
    }
}
