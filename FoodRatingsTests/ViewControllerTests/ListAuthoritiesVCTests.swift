//
//  AuthoritiesVCTests.swift
//  FoodRatings
//
//  Created by Waheed Malik on 21/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FoodRatings

extension ListAuthoritiesVCTests {
    class MockListAuthoritiesViewModel: ListAuthoritiesViewModel {
        func numberOfLocalAuthorities() -> Int {
            return 0
        }
        
        func localAuthority(at index: Int) -> LocalAuthorityViewModel? {
            return nil
        }
        
        var getLocalAuthoritiesCalled = false
        func getLocalAuthorities() {
            getLocalAuthoritiesCalled = true
        }
        
        func selectedLocalAuthority(at index: Int) {
            
        }
    }
}


class ListAuthoritiesVCTests: XCTestCase {
    var sut: ListAuthoritiesVC!
    
    override func setUp() {
        super.setUp()
        
        // Arrange
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        sut = storyBoard.instantiateViewController(withIdentifier: "ListAuthoritiesVC") as! ListAuthoritiesVC
    }
    
    func testAuthoritiesTableView_whenViewIsLoaded_isNotNil() {
        //Act
        _ = sut.view
        // Assert
        XCTAssertNotNil(sut.authoritiesTableView)
    }
    
    func testAuthoritiesTableView_whenViewIsLoaded_hasDataSource() {
        // Act
        sut.authoritiesTableViewDataSource = AuthoritiesTableViewDataSource()
        _ = sut.view
        // Assert
        XCTAssert(sut.authoritiesTableView.dataSource is AuthoritiesTableViewDataSource)
    }

    func testAuthoritiesTableView_whenViewIsLoaded_hasDelegate() {
        // Arrange
        sut.authoritiesTableViewDelegate = AuthoritiesTableViewDelegate()
        // Act
        _ = sut.view
        // Assert
        XCTAssert(sut.authoritiesTableView.delegate is AuthoritiesTableViewDelegate)
    }

    func testLoadLocalAuthorities_onViewLoad_shouldAskViewModelForLocalAuthorities() {
        // Arrange
        let mockViewModel = MockListAuthoritiesViewModel()
        sut.viewModel = mockViewModel
        //Act
        _ = sut.view
        // Assert
        XCTAssert(mockViewModel.getLocalAuthoritiesCalled)
    }

}
