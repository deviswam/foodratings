//
//  ListAuthoritiesViewModelTests.swift
//  FoodRatings
//
//  Created by Waheed Malik on 20/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import FoodRatings

extension ListAuthoritiesViewModelTests {
    class MockFoodRatingsManager: FoodRatingsManager {
        var completionHandler: (([LocalAuthority]?, FoodRatingsManagerError?) -> Void)?
        var loadLocalAuthoritiesCalled: Bool = false
        
        func loadLocalAuthorities(completionHandler: @escaping (_ localAuthorities: [LocalAuthority]?, _ error: FoodRatingsManagerError?) -> Void) {
            self.completionHandler = completionHandler
            self.loadLocalAuthoritiesCalled = true
        }
        
        func loadRatings(of localAuthorityId: Int, completionHandler: @escaping ([LocalAuthorityRating]?, FoodRatingsManagerError?) -> Void) {
            
        }
    }
    
    class MockListAuthoritiesViewModelViewDelegate: ListAuthoritiesViewModelViewDelegate {
        var expectation: XCTestExpectation?
        var authoritiesViewModelResult: ListAuthoritiesViewModelResult?
        
        func localAuthoritiesLoaded(with result: ListAuthoritiesViewModelResult) {
            guard let asynExpectation = expectation else {
                XCTFail("MockAuthoritiesViewModelViewDelegate was not setup correctly. Missing XCTExpectation reference")
                return
            }
            self.authoritiesViewModelResult = result
            asynExpectation.fulfill()
        }
    }
    
    class MockListAuthoritiesViewModelCoordinatorDelegate: ListAuthoritiesViewModelCoordinatorDelegate {
        func didSelect(localAuthority: LocalAuthority) {
            
        }
    }
}

class ListAuthoritiesViewModelTests: XCTestCase {
    var sut: FRListAuthoritiesViewModel!
    var mockFoodRatingsManager: MockFoodRatingsManager!
    var mockViewDelegate: MockListAuthoritiesViewModelViewDelegate!
    var mockCoordinatorDelegate: MockListAuthoritiesViewModelCoordinatorDelegate!
    
    override func setUp() {
        super.setUp()
        mockFoodRatingsManager = MockFoodRatingsManager()
        mockViewDelegate = MockListAuthoritiesViewModelViewDelegate()
        mockCoordinatorDelegate = MockListAuthoritiesViewModelCoordinatorDelegate()
        sut = FRListAuthoritiesViewModel(foodRatingsManager: mockFoodRatingsManager, viewDelegate: mockViewDelegate, coordinatorDelegate: mockCoordinatorDelegate)
    }
    
    func testGetLocalAuthorities_shouldAskFoodRatingsManagerForListOfAuthorities() {
        // Act
        sut.getLocalAuthorities()
        // Assert
        XCTAssert(mockFoodRatingsManager.loadLocalAuthoritiesCalled)
    }
    
    func testGetLocalAuthorities_whenAuthoritiesFound_raisesLocalAuthoritiesLoadedDelegateCallbackWithSuccess() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getLocalAuthorities()
        mockFoodRatingsManager.completionHandler?([FRLocalAuthority(id: 123)], nil)
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssert(self.mockViewDelegate.authoritiesViewModelResult! == .success)
        }
    }
    
    func testGetLocalAuthorities_whenNoAuthoritiesFound_raisesLocalAuthoritiesLoadedDelegateCallbackWithFailure() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getLocalAuthorities()
        mockFoodRatingsManager.completionHandler?(nil, FoodRatingsManagerError.noDataFoundError)
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssert(self.mockViewDelegate.authoritiesViewModelResult! == .failure)
        }
    }
    
    func testNumberOfLocalAuthorities_whenNoAuthoritiesFound_returnsZero() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getLocalAuthorities()
        mockFoodRatingsManager.completionHandler?(nil, FoodRatingsManagerError.noDataFoundError)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfLocalAuthorities(), 0)
        }
    }
    
    func testNumberOfLocalAuthorities_whenOneLocalAuthorityFound_returnsOne() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getLocalAuthorities()
        mockFoodRatingsManager.completionHandler?([FRLocalAuthority(id: 123)], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfLocalAuthorities(), 1)
        }
    }
    
    func testNumberOfLocalAuthorities_whenFiveLocalAuthoritiesFound_returnsFive() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getLocalAuthorities()
        mockFoodRatingsManager.completionHandler?([FRLocalAuthority(id: 123), FRLocalAuthority(id: 124), FRLocalAuthority(id: 125), FRLocalAuthority(id: 126), FRLocalAuthority(id: 127)], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfLocalAuthorities(), 5)
        }
    }
    
    func testAuthorityAtIndex_returnsTheCorrectLocalAuthorityViewModelObject() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getLocalAuthorities()
        mockFoodRatingsManager.completionHandler?([FRLocalAuthority(id: 123, name: "London"), FRLocalAuthority(id: 456, name: "Coventry")], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let localAuthorityLondon = self.sut.localAuthority(at: 0)
            let localAuthorityCoventry = self.sut.localAuthority(at: 1)
            XCTAssertEqual(localAuthorityLondon?.name, "London")
            XCTAssertEqual(localAuthorityCoventry?.name, "Coventry")
        }
    }
    
    func testAuthorityAtIndex_whenAuthorityNotFoundOnPassedIndex_returnsNil() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getLocalAuthorities()
        mockFoodRatingsManager.completionHandler?([FRLocalAuthority(id: 123, name: "London"), FRLocalAuthority(id: 456, name: "Coventry")], nil)
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let localAuthority = self.sut.localAuthority(at: 2)
            XCTAssertNil(localAuthority)
        }
    }
}
