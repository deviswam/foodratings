//
//  AuthoritiesTableViewDelegate.swift
//  FoodRatings
//
//  Created by Waheed Malik on 21/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class AuthoritiesTableViewDelegate : NSObject, UITableViewDelegate {
    var viewModel: ListAuthoritiesViewModel?
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.selectedLocalAuthority(at: indexPath.row)
    }
}
