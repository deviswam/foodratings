//
//  ListAuthoritiesCoordinator.swift
//  FoodRatings
//
//  Created by Waheed Malik on 20/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class AuthoritiesCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    fileprivate var navigationController: UINavigationController
    fileprivate var ratingsCoordinator: RatingsCoordinator?
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController) {
        self.navigationController = navController
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        if let listAuthoritiesVC = navigationController.topViewController as? ListAuthoritiesVC {
            let foodRatingsManager = SharedComponentsDir.foodRatingsManager
            let authoritiesTableViewDataSource = AuthoritiesTableViewDataSource()
            let authoritiesTableViewDelegate = AuthoritiesTableViewDelegate()
            let listAuthoritiesViewModel = FRListAuthoritiesViewModel(foodRatingsManager: foodRatingsManager, viewDelegate: listAuthoritiesVC, coordinatorDelegate: self)
            
            listAuthoritiesVC.viewModel = listAuthoritiesViewModel
            listAuthoritiesVC.authoritiesTableViewDataSource = authoritiesTableViewDataSource
            listAuthoritiesVC.authoritiesTableViewDelegate = authoritiesTableViewDelegate
            authoritiesTableViewDataSource.viewModel = listAuthoritiesViewModel
            authoritiesTableViewDelegate.viewModel = listAuthoritiesViewModel
        }
    }
}

extension AuthoritiesCoordinator : ListAuthoritiesViewModelCoordinatorDelegate {
    func didSelect(localAuthority: LocalAuthority) {
        ratingsCoordinator = RatingsCoordinator(navController: self.navigationController, localAuthority: localAuthority, delegate: self)
        ratingsCoordinator?.start()
    }
}

extension AuthoritiesCoordinator : RatingsCoordinatorDelegate {
    func ratingsCoordinatorDidFinish(ratingsCoordinator: RatingsCoordinator) {
        self.ratingsCoordinator = nil
    }
}

