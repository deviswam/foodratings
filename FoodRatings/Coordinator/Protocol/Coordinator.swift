//
//  Coordinator.swift
//  FoodRatings
//
//  Created by Waheed Malik on 22/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol Coordinator {
    func start()
}
