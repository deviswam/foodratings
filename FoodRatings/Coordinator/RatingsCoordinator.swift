//
//  RatingsCoordinator.swift
//  FoodRatings
//
//  Created by Waheed Malik on 22/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol RatingsCoordinatorDelegate: class {
    func ratingsCoordinatorDidFinish(ratingsCoordinator: RatingsCoordinator)
}

class RatingsCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    fileprivate var navigationController: UINavigationController
    fileprivate weak var delegate: RatingsCoordinatorDelegate!
    private var localAuthority: LocalAuthority
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController, localAuthority: LocalAuthority, delegate: RatingsCoordinatorDelegate) {
        self.navigationController = navController
        self.localAuthority = localAuthority
        self.delegate = delegate
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let ratingsVC = storyboard.instantiateViewController(withIdentifier: "RatingsVC") as? RatingsVC {
            let foodRatingsManager = SharedComponentsDir.foodRatingsManager
            let ratingsTableViewDataSource = RatingsTableViewDataSource()
            let ratingsViewModel = FRRatingsViewModel(selectedLocalAuthority: localAuthority, foodRatingsManager: foodRatingsManager, viewDelegate: ratingsVC, coordinatorDelegate: self)

            ratingsVC.viewModel = ratingsViewModel
            ratingsVC.ratingsTableViewDataSource = ratingsTableViewDataSource
            ratingsTableViewDataSource.viewModel = ratingsViewModel
            self.navigationController.pushViewController(ratingsVC, animated: true)
        }
    }
}

extension RatingsCoordinator: RatingsViewModelCoordinatorDelegate
{
    func didFinish() {
        self.navigationController.popViewController(animated: true)
        delegate.ratingsCoordinatorDidFinish(ratingsCoordinator: self)
    }
}
