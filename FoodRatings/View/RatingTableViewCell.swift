//
//  RatingTableViewCell.swift
//  FoodRatings
//
//  Created by Waheed Malik on 23/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol RatingTableViewCell {
    func configCell(with localAuthorityRatingViewModel: LocalAuthorityRatingViewModel)
}

class FRRatingTableViewCell: UITableViewCell, RatingTableViewCell {
    
    @IBOutlet weak var ratingNameLabel: UILabel!
    @IBOutlet weak var ratingPercentageLabel: UILabel!
    
    func configCell(with localAuthorityRatingViewModel: LocalAuthorityRatingViewModel) {
        ratingNameLabel.text = localAuthorityRatingViewModel.ratingName
        ratingPercentageLabel.text = localAuthorityRatingViewModel.ratingPercentage
    }
}
