//
//  AuthorityTableViewCell.swift
//  FoodRatings
//
//  Created by Waheed Malik on 21/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol AuthorityTableViewCell {
    func configCell(with authorityViewModel: LocalAuthorityViewModel)
}

class FRAuthorityTableViewCell: UITableViewCell, AuthorityTableViewCell {
    
    @IBOutlet weak var authorityNameLabel: UILabel!
    
    func configCell(with authorityViewModel: LocalAuthorityViewModel) {
        authorityNameLabel.text = authorityViewModel.name
    }
}
