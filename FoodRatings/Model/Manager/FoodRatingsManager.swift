//
//  FoodRatingsManager.swift
//  FoodRatings
//
//  Created by Waheed Malik on 20/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum FoodRatingsManagerError : Error {
    case noDataFoundError
    case connectionError
    
    func userFriendlyMessage() -> String {
        switch self {
        case .noDataFoundError:
            return "Unable to fetch requested data"
        case .connectionError:
            return "Connection error"
        }
    }
}

protocol FoodRatingsManager {
    // gets the list of all authorities
    func loadLocalAuthorities(completionHandler: @escaping (_ localAuthorities: [LocalAuthority]?, _ error: FoodRatingsManagerError?) -> Void)
    // gets the ratings of an authority
    func loadRatings(of localAuthorityId: Int, completionHandler: @escaping (_ ratings: [LocalAuthorityRating]?, _ error: FoodRatingsManagerError?) -> Void)
}

class FRFoodRatingsManager: FoodRatingsManager {
    // MARK:- PRIVATE VARIABLES
    private let foodRatingsAPI: FoodRatingsAPI
    
    // MARK:- INITIALIZER
    init(foodRatingsAPI: FoodRatingsAPI) {
        self.foodRatingsAPI = foodRatingsAPI
    }
    
    // MARK:- PUBLIC METHODS
    func loadLocalAuthorities(completionHandler: @escaping (_ localAuthorities: [LocalAuthority]?, _ error: FoodRatingsManagerError?) -> Void) {
        foodRatingsAPI.fetchLocalAuthorities { [weak self] (localAuthorities: [LocalAuthority]?, error: FoodRatingsAPIError?) in
            var managerError: FoodRatingsManagerError?
            if let error = error {
                managerError = self?.foodRatingsManagerError(from: error)
            }
            completionHandler(localAuthorities, managerError)
        }
    }
    
    func loadRatings(of localAuthorityId: Int, completionHandler: @escaping (_ ratings: [LocalAuthorityRating]?, _ error: FoodRatingsManagerError?) -> Void) {
        foodRatingsAPI.fetchEstablishments(of: localAuthorityId) { [weak self] (establishments: [Establishment]?, error: FoodRatingsAPIError?) in
            var managerError: FoodRatingsManagerError?
            var ratings: [LocalAuthorityRating]?
            if let error = error {
                managerError = self?.foodRatingsManagerError(from: error)
            } else if let establishments = establishments {
                ratings = self?.generateRatings(from: establishments)
            }
            completionHandler(ratings, managerError)
        }
    }
    
    // MARK:- PRIVATE METHODS
    private func generateRatings(from localAuthorityEstablishments: [Establishment]) -> [LocalAuthorityRating]? {
        // get the number of establishments rated against each Rating value. (i.e: "1": 5.0, "2": 4.0)
        var ratings = [String: Int]()
        localAuthorityEstablishments.forEach { establishment in
            if let ratingValue = establishment.ratingValue {
                if ratings[ratingValue] != nil {
                    ratings[ratingValue] = ratings[ratingValue]! + 1
                } else {
                    ratings[ratingValue] = 1
                }
            }
        }
        
        // convert ratings into percentage and create LocalAuthorityRating objects collection.
        var localAuthorityRatings: [LocalAuthorityRating]?
        localAuthorityRatings = ratings.map { (ratingValue: String, numberOfEstablishments: Int) -> LocalAuthorityRating in
            let percentage = Float(numberOfEstablishments)/Float(localAuthorityEstablishments.count) * 100.0
            return FRLocalAuthorityRating(ratingValue: ratingValue, percentageOfEstablishmentsInvolved: roundf(percentage))
        }
        
        let sortedRatings = self.sortRatings(from: localAuthorityRatings)
        return sortedRatings
    }
    
    private func sortRatings(from unsortedRatings: [LocalAuthorityRating]?) -> [LocalAuthorityRating]? {
        //filter out the integer rating values
        let integerRatings = unsortedRatings?.filter({ rating -> Bool in
            return Int(rating.ratingValue) != nil
        }).sorted(by: { (lhsRating, rhsRating) -> Bool in
            return lhsRating.ratingValue > rhsRating.ratingValue
        })
        
        //filter out the non-integer rating values
        let nonIntegerRatings = unsortedRatings?.filter({ rating -> Bool in
            return Int(rating.ratingValue) == nil
        }).sorted(by: { (lhsRating, rhsRating) -> Bool in
            return lhsRating.ratingValue > rhsRating.ratingValue
        })
        
        let sortedRatings = integerRatings! + nonIntegerRatings!
        return sortedRatings
    }
    
    private func foodRatingsManagerError(from ratingsAPIError: FoodRatingsAPIError) -> FoodRatingsManagerError? {
        switch ratingsAPIError {
        case .dataSerializationError, .invalidDataError:
            return FoodRatingsManagerError.noDataFoundError
        case .httpError:
            return FoodRatingsManagerError.connectionError
        }
    }
}
