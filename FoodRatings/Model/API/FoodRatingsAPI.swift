//
//  FoodRatingsAPI.swift
//  FoodRatings
//
//  Created by Waheed Malik on 21/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum FoodRatingsAPIError: Error {
    case httpError
    case invalidDataError
    case dataSerializationError
}

enum FoodRatingsAPIMethod: String {
    case basicListOfAuthorities = "Authorities/basic"
    case establishments = "Establishments"
}

protocol FoodRatingsAPI {
    // fetch all local authorities
    func fetchLocalAuthorities(completionHandler: @escaping (_ localAuthorities: [LocalAuthority]?, _ error: FoodRatingsAPIError?) -> Void)
    // fetches all the establishments of an authority
    func fetchEstablishments(of localAuthorityId: Int, completionHandler: @escaping (_ establishments: [Establishment]?, _ error: FoodRatingsAPIError?) -> Void)
}

class FRFoodRatingsAPI: FoodRatingsAPI {
    // MARK:- PRIVATE VARIABLES
    private let BASE_URL = "http://api.ratings.food.gov.uk/"
    private let commonRequestHeaders = ["x-api-version":"2"]

    private let urlSession: URLSession!
    
    // MARK:- INITIALIZER
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    // MARK:- PUBLIC METHODS
    func fetchLocalAuthorities(completionHandler: @escaping (_ localAuthorities: [LocalAuthority]?, _ error: FoodRatingsAPIError?) -> Void) {
        guard let urlRequest = self.createURLRequest(with: .basicListOfAuthorities) else {
            return
        }
        self.getLocalAuthorities(with: urlRequest) { (localAuthorities, error) in
            completionHandler(localAuthorities, error)
        }
    }
    
    func fetchEstablishments(of localAuthorityId: Int, completionHandler: @escaping (_ establishments: [Establishment]?, _ error: FoodRatingsAPIError?) -> Void) {
        let requestParams = ["localAuthorityId": String(localAuthorityId),
                             "pageNumber": "0"]
        guard let urlRequest = self.createURLRequest(with: .establishments, parameters:requestParams) else {
            return
        }
        self.getEstablishmentsOfAnAuthority(with: urlRequest) { (establishments, error) in
            completionHandler(establishments, error)
        }
    }
    
    // MARK:- PRIVATE METHODS
    private func getEstablishmentsOfAnAuthority(with urlRequest: URLRequest, completionHandler: @escaping (_ establishments: [Establishment]?, _ error: FoodRatingsAPIError?) -> Void) {
        let dataTask = urlSession.dataTask(with: urlRequest) { [weak self] (data, urlResponse, error) in
            var establishments : [Establishment]?
            var retError : FoodRatingsAPIError?
            
            if error != nil {
                retError = FoodRatingsAPIError.httpError
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        guard let establishmentsArray = dictionary["establishments"] as? [[String : AnyObject]] else {
                            retError = FoodRatingsAPIError.invalidDataError
                            DispatchQueue.main.async {
                                completionHandler(establishments, retError)
                            }
                            return
                        }
                        establishments = self?.createEstablishments(from: establishmentsArray)
                    }
                } catch {
                    retError = FoodRatingsAPIError.dataSerializationError
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(establishments, retError)
            }
        }
        dataTask.resume()
    }
    
    private func getLocalAuthorities(with urlRequest: URLRequest, completionHandler: @escaping (_ localAuthorities: [LocalAuthority]?, _ error: FoodRatingsAPIError?) -> Void) {
        let dataTask = urlSession.dataTask(with: urlRequest) { [weak self] (data, urlResponse, error) in
            var localAuthorities : [LocalAuthority]?
            var retError : FoodRatingsAPIError?
            
            if error != nil {
                retError = FoodRatingsAPIError.httpError
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        guard let authoritiesArray = dictionary["authorities"] as? [[String : AnyObject]] else {
                            retError = FoodRatingsAPIError.invalidDataError
                            DispatchQueue.main.async {
                                completionHandler(localAuthorities, retError)
                            }
                            return
                        }
                        localAuthorities = self?.createLocalAuthorities(from: authoritiesArray)
                    }
                } catch {
                    retError = FoodRatingsAPIError.dataSerializationError
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(localAuthorities, retError)
            }
        }
        dataTask.resume()
    }
    
    private func createLocalAuthorities(from authoritiesArray:[[String : AnyObject]]) -> [LocalAuthority]? {
        var localAuthorities: [LocalAuthority]?
        localAuthorities = authoritiesArray.map { (authorityDictionary: [String : AnyObject]) -> LocalAuthority? in
                return FRLocalAuthority(dictionary: authorityDictionary)
            }.filter { (localAuthority: LocalAuthority?) -> Bool in
                return localAuthority != nil
        } as? [LocalAuthority]
        
        return localAuthorities
    }
    
    private func createEstablishments(from establishmentsArray:[[String : AnyObject]]) -> [Establishment]? {
        var establishments: [Establishment]?
        establishments = establishmentsArray.map { (establishmentDictionary: [String : AnyObject]) -> Establishment? in
            return FREstablishment(dictionary: establishmentDictionary)
            }.filter { (establishment: Establishment?) -> Bool in
                return establishment != nil
            } as? [Establishment]
        
        return establishments
    }

    
    private func createURLRequest(with apiMethod:FoodRatingsAPIMethod, headers:[String: String] = [:], parameters:[String: String] = [:]) -> URLRequest? {
        // create url
        var urlComponents = URLComponents(string: BASE_URL + apiMethod.rawValue)
        
        // append parameters
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            let query = URLQueryItem(name: key, value: value)
            queryItems.append(query)
        }
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else { return nil }
        print("WAM URL:\(url.absoluteString)")
        
        // create request object
        var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15)
        
        // append request header values
        commonRequestHeaders.forEach { request.addValue($1, forHTTPHeaderField: $0)}
        headers.forEach { request.addValue($1, forHTTPHeaderField: $0) }
        
        return request
    }
}
