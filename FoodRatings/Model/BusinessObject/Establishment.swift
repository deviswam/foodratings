//
//  Establishment.swift
//  FoodRatings
//
//  Created by Waheed Malik on 20/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol Establishment {
    var id: Int { get }
    var ratingValue: String? { get }
}

struct FREstablishment: Establishment, ObjectMapper {
    var id: Int
    var ratingValue: String?
    
    init(id: Int, ratingValue: String? = nil) {
        self.id = id
        self.ratingValue = ratingValue
    }
    
    init?(dictionary: Dictionary<String, Any>?) {
        guard let dictionary = dictionary,
            let dId = dictionary["FHRSID"] as? Int else {
                return nil
        }
        let dRatingValue = dictionary["RatingValue"] as? String
        
        self.init(id: dId, ratingValue: dRatingValue)
    }
}

func == (lhs: Establishment, rhs: Establishment) -> Bool {
    if lhs.id == rhs.id {
        return true
    }
    return false
}
