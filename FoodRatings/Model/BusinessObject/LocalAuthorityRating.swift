//
//  LocalAuthorityRating.swift
//  FoodRatings
//
//  Created by Waheed Malik on 22/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol LocalAuthorityRating {
    var ratingValue: String { get }
    var percentageOfEstablishmentsInvolved: Float { get }
}

class FRLocalAuthorityRating: LocalAuthorityRating {
    var ratingValue: String
    var percentageOfEstablishmentsInvolved: Float
    
    init(ratingValue: String, percentageOfEstablishmentsInvolved: Float) {
        self.ratingValue = ratingValue
        self.percentageOfEstablishmentsInvolved = percentageOfEstablishmentsInvolved
    }
}
