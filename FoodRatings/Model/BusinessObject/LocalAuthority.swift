//
//  LocalAuthority.swift
//  FoodRatings
//
//  Created by Waheed Malik on 20/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum SchemeType: Int {
    case FHRS = 1
    case FHIS = 2
}

protocol LocalAuthority {
    var id: Int { get }
    var name: String? { get }
    var schemeType: SchemeType? { get }
    var ratings: [LocalAuthorityRating]? { get set }
}

struct FRLocalAuthority: LocalAuthority, ObjectMapper {
    var id: Int
    var name: String?
    var ratings: [LocalAuthorityRating]?
    var schemeType: SchemeType?
    
    init(id: Int, name: String? = nil, schemeType: SchemeType? = nil) {
        self.id = id
        self.name = name
        self.schemeType = schemeType
    }
    
    init?(dictionary: Dictionary<String, Any>?) {
        guard let dictionary = dictionary,
            let dId = dictionary["LocalAuthorityId"] as? Int else {
                return nil
        }
        let dName = dictionary["Name"] as? String
        let dSchemeType = dictionary["SchemeType"] as? SchemeType

        self.init(id: dId, name: dName, schemeType:dSchemeType)
    }
}

func == (lhs: LocalAuthority, rhs: LocalAuthority) -> Bool {
    if lhs.id == rhs.id {
        return true
    }
    return false
}
