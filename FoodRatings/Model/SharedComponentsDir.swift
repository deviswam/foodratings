//
//  SharedComponentsDirectory.swift
//  FoodRatings
//
//  Created by Waheed Malik on 22/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

class SharedComponentsDir {
    static let foodRatingsManager: FoodRatingsManager = {
        let foodRatingsManager = FRFoodRatingsManager(foodRatingsAPI: SharedComponentsDir.foodRatingsAPI)
        return foodRatingsManager
    }()
    
    static let foodRatingsAPI: FoodRatingsAPI = {
        let foodRatingsAPI = FRFoodRatingsAPI()
        return foodRatingsAPI
    }()
}
