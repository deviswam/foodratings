//
//  RatingsTableViewDataSource.swift
//  FoodRatings
//
//  Created by Waheed Malik on 23/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class RatingsTableViewDataSource: NSObject, UITableViewDataSource {
    var viewModel: RatingsViewModel?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfRatings()
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RatingTableViewCell", for: indexPath)
        if let ratingCell = cell as? RatingTableViewCell,
            let rating = viewModel?.rating(at: indexPath.row) {
            ratingCell.configCell(with: rating)
        }
        return cell
    }
}
