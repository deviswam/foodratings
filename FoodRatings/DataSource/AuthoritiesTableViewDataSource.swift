//
//  AuthoritiesTableViewDataSource.swift
//  FoodRatings
//
//  Created by Waheed Malik on 21/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class AuthoritiesTableViewDataSource: NSObject, UITableViewDataSource {
    var viewModel: ListAuthoritiesViewModel?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfLocalAuthorities()
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AuthorityTableViewCell", for: indexPath)
        if let authorityCell = cell as? AuthorityTableViewCell,
            let localAuthority = viewModel?.localAuthority(at: indexPath.row) {
            authorityCell.configCell(with: localAuthority)
        }
        return cell
    }
}
