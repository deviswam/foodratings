//
//  RatingsVC.swift
//  FoodRatings
//
//  Created by Waheed Malik on 22/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class RatingsVC: UIViewController {
    @IBOutlet weak var ratingsTableView: UITableView!
    @IBOutlet weak var ratingsTitleLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel: RatingsViewModel?
    var ratingsTableViewDataSource: UITableViewDataSource?
    var ratingsTableViewDelegate: UITableViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ratingsTableView.dataSource = ratingsTableViewDataSource
        ratingsTableView.delegate = ratingsTableViewDelegate
        self.activityIndicator.startAnimating()
        self.ratingsTableView.isHidden = true
        
        // get ratings of selected authority
        viewModel?.getRatingsOfSelectedAuthority()
        // set ratings title
        self.ratingsTitleLabel.text = viewModel?.ratingsTitle()
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        viewModel?.navigateBack()
    }
}

extension RatingsVC: RatingsViewModelViewDelegate {
    func ratingsLoaded(with result: RatingsViewModelResult) {
        switch result {
        case .success:
            self.ratingsTableView.reloadData()
        case .failure:
            print("Info (Inform user): Unable to fetch Ratings of the selected Authority")
        }
        self.ratingsTableView.isHidden = false
        self.activityIndicator.stopAnimating()
    }
}
