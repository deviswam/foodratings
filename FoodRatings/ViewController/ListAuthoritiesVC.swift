//
//  ViewController.swift
//  FoodRatings
//
//  Created by Waheed Malik on 20/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class ListAuthoritiesVC: UIViewController {
    @IBOutlet weak var authoritiesTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var authoritiesTableViewDataSource: UITableViewDataSource?
    var authoritiesTableViewDelegate: UITableViewDelegate?
    var viewModel: ListAuthoritiesViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        authoritiesTableView.dataSource = authoritiesTableViewDataSource
        authoritiesTableView.delegate = authoritiesTableViewDelegate
        self.activityIndicator.startAnimating()
        self.authoritiesTableView.isHidden = true
        
        // Call to load authorities
        viewModel?.getLocalAuthorities()
    }
}

extension ListAuthoritiesVC: ListAuthoritiesViewModelViewDelegate {
    func localAuthoritiesLoaded(with result: ListAuthoritiesViewModelResult) {
        switch result {
        case .success:
            self.authoritiesTableView.reloadData()
        case .failure:
            print("Info (Inform user): Unable to fetch Local Authorities list")
        }
        self.activityIndicator.stopAnimating()
        self.authoritiesTableView.isHidden = false
    }
}

