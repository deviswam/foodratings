//
//  RatingViewModel.swift
//  FoodRatings
//
//  Created by Waheed Malik on 22/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol LocalAuthorityRatingViewModel {
    var ratingName: String { get }
    var ratingPercentage: String { get }
}

class FRLocalAuthorityRatingViewModel: LocalAuthorityRatingViewModel {
    var ratingName: String
    var ratingPercentage: String
    init(name: String, percentage: Float) {
        self.ratingName = Int(name) != nil ? "\(name)-star" : name
        self.ratingPercentage = "\(Int(percentage)) %"
    }
}
