//
//  RatingsViewModel.swift
//  FoodRatings
//
//  Created by Waheed Malik on 22/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum RatingsViewModelResult : Equatable{
    case success
    case failure
}

// MARK:- VIEW DELEGATE PROTOCOL
protocol RatingsViewModelViewDelegate: class {
    func ratingsLoaded(with result: RatingsViewModelResult)
}

// MARK:- COORDINATOR DELEGATE PROTOCOL
protocol RatingsViewModelCoordinatorDelegate: class {
    func didFinish()
}

// MARK:- VIEWMODEL PROTOCOL
protocol RatingsViewModel {
    // Accessor methods
    func getRatingsOfSelectedAuthority()
    func ratingsTitle() -> String
    func numberOfRatings() -> Int
    func rating(at index: Int) -> LocalAuthorityRatingViewModel?
    
    // Navigation methods
    func navigateBack()
}

class FRRatingsViewModel : RatingsViewModel {
    // MARK: PRIVATE VARIABLES
    private var localAuthority: LocalAuthority
    private let foodRatingsManager: FoodRatingsManager
    private var viewDelegate: RatingsViewModelViewDelegate!
    private weak var coordinatorDelegate: RatingsViewModelCoordinatorDelegate!
    
    // MARK: INITIALIZER
    init(selectedLocalAuthority: LocalAuthority, foodRatingsManager: FoodRatingsManager, viewDelegate: RatingsViewModelViewDelegate, coordinatorDelegate: RatingsViewModelCoordinatorDelegate) {
        self.localAuthority = selectedLocalAuthority
        self.foodRatingsManager = foodRatingsManager
        self.viewDelegate = viewDelegate
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    // MARK: PUBLIC METHODS
    func navigateBack() {
        coordinatorDelegate.didFinish()
    }
    
    func getRatingsOfSelectedAuthority() {
        var result: RatingsViewModelResult = .failure
        self.foodRatingsManager.loadRatings(of: localAuthority.id) { (ratings: [LocalAuthorityRating]?, error: FoodRatingsManagerError?) in
            if let ratings = ratings, error == nil {
                result = .success
                self.localAuthority.ratings = ratings
                //print("\(ratings)")
            }
            self.viewDelegate.ratingsLoaded(with: result)
        }
    }
    
    func ratingsTitle() -> String {
        guard let localAuthorityName = self.localAuthority.name else { return ""}
        return "Hygiene ratings of \(localAuthorityName)"
    }
    
    func numberOfRatings() -> Int {
        guard let ratings = localAuthority.ratings else { return 0 }
        return ratings.count
    }
    
    func rating(at index: Int) -> LocalAuthorityRatingViewModel? {
        if let ratings = localAuthority.ratings, index < ratings.count {
            let rating = ratings[index]
            return FRLocalAuthorityRatingViewModel(name: rating.ratingValue, percentage: rating.percentageOfEstablishmentsInvolved)
        }
        return nil
    }
}
