//
//  LocalAuthorityViewModel.swift
//  FoodRatings
//
//  Created by Waheed Malik on 21/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol LocalAuthorityViewModel {
    var name: String { get }
}

class FRLocalAuthorityViewModel: LocalAuthorityViewModel {
    var name: String = ""
    init(localAuthority: LocalAuthority) {
        if let name = localAuthority.name {
            self.name = name
        }
    }
}
