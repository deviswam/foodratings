//
//  ListAuthoritiesViewModel.swift
//  FoodRatings
//
//  Created by Waheed Malik on 20/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum ListAuthoritiesViewModelResult : Equatable{
    case success
    case failure
}

// MARK:- VIEW DELEGATE PROTOCOL
protocol ListAuthoritiesViewModelViewDelegate: class {
    func localAuthoritiesLoaded(with result: ListAuthoritiesViewModelResult)
}

// MARK:- COORDINATOR DELEGATE PROTOCOL
protocol ListAuthoritiesViewModelCoordinatorDelegate: class {
    func didSelect(localAuthority: LocalAuthority)
}

// MARK:- VIEWMODEL PROTOCOL
protocol ListAuthoritiesViewModel {
    func numberOfLocalAuthorities() -> Int
    func localAuthority(at index: Int) -> LocalAuthorityViewModel?
    func selectedLocalAuthority(at index: Int)
    
    func getLocalAuthorities()
}

class FRListAuthoritiesViewModel: ListAuthoritiesViewModel {
    // MARK: PRIVATE VARIABLES
    private let foodRatingsManager: FoodRatingsManager
    private weak var viewDelegate: ListAuthoritiesViewModelViewDelegate!
    private weak var coordinatorDelegate: ListAuthoritiesViewModelCoordinatorDelegate!
    private var localAuthorities: [LocalAuthority]?
    
    // MARK: INITIALIZER
    init(foodRatingsManager: FoodRatingsManager, viewDelegate: ListAuthoritiesViewModelViewDelegate, coordinatorDelegate: ListAuthoritiesViewModelCoordinatorDelegate) {
        self.foodRatingsManager = foodRatingsManager
        self.viewDelegate = viewDelegate
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    // MARK: PUBLIC METHODS
    func getLocalAuthorities() {
        var result: ListAuthoritiesViewModelResult = .failure
        foodRatingsManager.loadLocalAuthorities { (localAuthorities: [LocalAuthority]?, error: FoodRatingsManagerError?) in
            if let localAuthorities = localAuthorities, error == nil {
                result = .success
                self.localAuthorities = localAuthorities
            }
            self.viewDelegate.localAuthoritiesLoaded(with: result)
        }
    }
    
    func numberOfLocalAuthorities() -> Int {
        guard let localAuthorities = localAuthorities else { return 0 }
        return localAuthorities.count
    }
    
    func localAuthority(at index: Int) -> LocalAuthorityViewModel? {
        if let localAuthorities = localAuthorities, index < localAuthorities.count {
            return FRLocalAuthorityViewModel(localAuthority: localAuthorities[index])
        }
        return nil
    }
    
    func selectedLocalAuthority(at index: Int) {
        guard let localAuthorities = localAuthorities, index < localAuthorities.count else {
            return
        }
        self.coordinatorDelegate.didSelect(localAuthority: localAuthorities[index])
    }
}
