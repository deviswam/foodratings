IMPORTANT NOTES
===============

1. The project fully implements the requested user story with the acceptance criteria.
2. iOS 9 onwards supported. Tested on iPhone 5 onwards devices in both orientation. 
3. The project has been developed on latest xcode 8.3.2 without any warnings or errors.
4. Attached is the test coverage snapshot. Project Test coverage is reported as 46 % by Xcode based on 44 test cases.
5. The app is following MVVM-C (Model-View-ViewModel-Coordinator) architecture pattern. 
6. TDD approach has been adopted for development with XCTest.
7. I preferred to use all the built-in native iOS technologies and not using third-party libraries or CocoaPods for this small project.
8. With more time, following things could be implemented/improved:
	* Aiming to achieve 100% test coverage by writing test cases for ratings screen and its related MVVM-C module components. Most of those test cases would be same as ListAuthorities screen MVVM-C module, whose test cases are already written. 
    * Error handling test cases on FoodRatingsManager, FoodRatingsAPI and ViewModel classes. I was aiming to complete the happy path first. Despite of that application code is stable and doesn't crash on httpError and serialization scenarios for example.
	* Test cases are not refactored to follow DRY principle hence there would be some repetition in few test cases.
	* There are no functional tests written because of limited time. I wish i could have the chance to write them using KIF or iOS UITesting. I've written them in past.
    * User info messages are currently being shown in Xcode console incase of any issue with retriving data, this could be improved by showing alert to the user.
    * Storing an authority ratings once retrieved so that next time it doesn't need downloading when same authority is selected during same session.
9. This project is built on best software engineering practices. To name a few: SOLID principles, composition over inheritance, protocol oriented programming and loosely coupled architecture.

Thanks for your time.
